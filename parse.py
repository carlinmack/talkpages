"""
This script allows the user to parse a dump from a database connection
and extract features to a database table.

This tool uses a mysql database that is configured in the parse() function.

Please run pip install -r requirements.txt before running this script.
"""

import os
import re
import subprocess
import sys
from datetime import datetime

import mwxml
import mysql.connector as sql
import tqdm
from mysql.connector import errorcode


def databaseConnect():
    """Connect to MySQL database using password stored in options file

    Returns
    -------
    database: MySQLConnection - connection to the MySQL DB
    cursor: MySQLCursor - cursor allowing CRUD actions on the DB connections
    """
    try:
        database = sql.connect(
            host="wikiactors.cs.virginia.edu",
            database="wikiactors",
            username="wikiactors",
            option_files="private.cnf",
            option_groups="wikiactors",
            autocommit="true",
        )

        cursor = database.cursor()

    except sql.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
        raise
    else:
        return database, cursor


def getDump(cursor):
    """Returns the next dump to be parsed from the database

    Parameters
    ----------
    cursor: MySQLCursor - cursor allowing CRUD actions on the DB connections

    Returns
    -------
    dump: class 'mwxml.iteration.dump.Dump' - dump file iterator
    filename: str - filename of dump
    """
    ## Read dump from database
    query = "SELECT file_name FROM partition WHERE status = 'todo' LIMIT 1"
    cursor.execute(query)
    todofile = cursor.fetchone()

    if not todofile:
        ## no files to run, close database connections and finish
        return None, None

    filename = todofile[0]
    path = "partitions/" + filename

    if not os.path.exists(path):
        raise IOError("file not found on disk")

    print(path)
    dump = mwxml.Dump.from_file(open(path))

    ## Change status of dump
    currenttime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    query = """UPDATE partition
        SET
            status = "running",
            start_time_1 = %s
        WHERE file_name = %s;"""
    cursor.execute(query, (currenttime, filename))

    return dump, filename


##  FUNCTIONS TO EXTRACT FEATURES
def cleanString(string: str):
    """Removes special characters and unnecessary whitespace from text"""
    removeSymbols = re.sub(r'[$-/:-?{-~!"^_`\[\]]', " ", string)
    removeDoubleSpaces = re.sub(r"\s\s+", " ", removeSymbols)
    return removeDoubleSpaces


def longestWord(string: str):
    """Returns the length of the longest word in text"""
    string = cleanString(string)
    arr = string.split()
    if len(arr) > 0:
        longestWord = max(arr, key=len)
        # print(longestWord)
        return len(longestWord)
    else:
        return 0


def longestCharSequence(string: str):
    """Returns the length of the longest repeated character sequence in text"""
    string = cleanString(string)
    # print(string)
    previous = ""
    current = 0
    maxLength = 0
    temp = []

    for char in string:
        if char == previous:
            current += 1
            temp.append(char)
        else:
            if current > maxLength:
                maxLength = current
            current = 1
            previous = char

    return maxLength


def ratioCapitals(string: str):
    """Returns the ratio of uppercase to lowercase characters in text"""
    uppercase = 0
    lowercase = 1  # to avoid infinity

    # print(string)

    for char in string:
        if ord(char) >= 65 and ord(char) <= 90:
            uppercase = uppercase + 1
        elif ord(char) >= 97 and ord(char) <= 122:
            lowercase = lowercase + 1

    return uppercase / lowercase


def ratioDigits(string: str):
    """Returns the ratio of digits to all characters in text"""
    digits = 0

    for char in string:
        if char.isdigit():
            digits = digits + 1

    return digits / len(string)


def ratioSpecial(string: str):
    """Returns the ratio of special characters to all characters in text"""
    return len(re.findall(r'[!-/:-?{-~!"^_`\[\]]', string)) / len(string)


def ratioWhitespace(string: str):
    """Returns the ratio of whitespace to all characters in text"""
    return len(re.findall(r"\s", string)) / len(string)


def ratioPronouns(string: str):
    """Returns the ratio of personal pronouns to all words in text"""
    return len(re.findall(r"(\sI\s|\sme\s|\smy\s|\smine\s|\smyself\s)", string)) / len(
        string.split(" ")
    )


def getDiff(old: str, new: str):
    """Returns the diff between two edits using wdiff

    Parameters
    ----------
    old : str - old revision
    new : str - new revision

    Returns
    -------
    added: str - all the text that is exclusively in the new revision
    deleted: str - all the text that is exclusively in the old revision
    """
    first = "oldrevision.txt"
    with open(first, "w") as oldfile:
        oldfile.writelines(old)

    second = "newrevision.txt"
    with open(second, "w") as newfile:
        newfile.writelines(new)

    removelines = (
        "======================================================================"
    )

    added = (
        subprocess.run(["wdiff", "-13", first, second], capture_output=True)
        .stdout.decode("utf-8")
        .strip()
    )

    added = re.sub(removelines, "", added)

    deleted = (
        subprocess.run(["wdiff", "-23", first, second], capture_output=True)
        .stdout.decode("utf-8")
        .strip()
    )

    deleted = re.sub(removelines, "", deleted)

    return added, deleted


##	PRINT FEATURES FOR EVERY PAGE
def parse():
    """Selects the next dump from the database, extracts the features and
    imports them into several database tables.
    """
    database, cursor = databaseConnect()

    if database is None:
        cursor.close()
        database.close()
        return

    try:
        dump, filename = getDump(cursor)

        ## Parse
        for page in dump:
            namespace = page.namespace
            title = page.title
            query = """INSERT IGNORE INTO page (page_id, namespace, title, file_name)
                VALUES (%s, %s, %s, %s)"""
            cursor.execute(query, (page.id, namespace, title, filename))
            database.commit()

            if namespace != 1:
                continue

            oldText = ""
            for revision in tqdm.tqdm(page, desc=title, unit=" edits"):
                ## Page Features
                if not revision.user:
                    continue

                if revision.user.id:
                    userId = revision.user.id
                    ipAddress = "NULL"

                    query = """INSERT INTO user (user_id, username, namespaces)
                        VALUES (%s, %s, %s) ON DUPLICATE KEY
                        UPDATE
                            namespaces = CONCAT_WS(',', namespaces, %s),
                            number_of_edits = number_of_edits + 1;"""
                    cursor.execute(
                        query, (userId, revision.user.text, namespace, str(namespace),),
                    )
                    userTableId = cursor.lastrowid
                else:
                    userId = "NULL"
                    ipAddress = revision.user.text

                    query = """INSERT INTO user (ip_address, namespaces)
                        VALUES (%s, %s) ON DUPLICATE KEY
                        UPDATE
                            namespaces = CONCAT_WS(',', namespaces, %s),
                            number_of_edits = number_of_edits + 1;"""
                    cursor.execute(query, (ipAddress, namespace, str(namespace)))
                    userTableId = cursor.lastrowid

                editDate = datetime.strptime(
                    str(revision.timestamp), "%Y-%m-%dT%H:%M:%SZ"
                )

                editId = revision.id
                pageId = revision.page.id

                # if revision has text and the text isn't whitespace
                if revision.text and not re.search(r"^\s+$", revision.text):
                    blanking = False
                    (added, deleted) = getDiff(oldText, revision.text)
                    blankAddition = re.search(r"^\s+$", added)

                    if added and not blankAddition:
                        insInternalLink = len(re.findall(r"\[\[.*?\]\]", added))
                        insExternalLink = len(
                            re.findall(r"[^\[]\[[^\[].*?[^\]]\][^\]]", added)
                        )

                        insLongestInsertedWord = longestWord(added)
                        insLongestCharacterSequence = longestCharSequence(added)

                        insCapitalization = ratioCapitals(added)
                        insDigits = ratioDigits(added)
                        insSpecialChars = ratioSpecial(added)
                        insWhitespace = ratioWhitespace(added)

                        insPronouns = ratioPronouns(added)
                    else:
                        insInternalLink = 0
                        insExternalLink = 0

                        insLongestInsertedWord = 0
                        insLongestCharacterSequence = 0

                        insCapitalization = 0
                        insDigits = 0
                        insSpecialChars = 0
                        if blankAddition:
                            insWhitespace = 1
                        else:
                            insWhitespace = 0

                        insPronouns = 0

                    delWords = len(deleted.split(" "))

                    oldText = revision.text
                else:
                    blanking = True

                    insInternalLink = "NULL"
                    insExternalLink = "NULL"
                    insLongestInsertedWord = "NULL"
                    insLongestCharacterSequence = "NULL"
                    insCapitalization = "NULL"
                    insDigits = "NULL"
                    insSpecialChars = "NULL"

                if revision.comment:
                    comment = revision.comment.lower()
                    commentCopyedit = "copyedit" in comment
                    commentPersonalLife = "personal life" in comment
                    commentLength = len(comment)
                    commentSpecialChars = ratioSpecial(comment)
                else:
                    commentCopyedit = "NULL"
                    commentPersonalLife = "NULL"
                    commentLength = "NULL"
                    commentSpecialChars = "NULL"

                query = """
                INSERT INTO edit (added, deleted, edit_date, 
                    edit_id, page_id, ins_internal_link, ins_external_link, 
                    ins_longest_inserted_word, ins_longest_character_sequence, 
                    ins_pronouns, ins_capitalization, ins_digits, ins_special_chars, 
                    ins_whitespace, del_words, comment_personal_life, comment_copyedit, 
                    comment_length, comment_special_chars, blanking, user_table_id
                ) VALUES (
                    %s, %s, %s,
                    %s, %s, %s, %s,
                    %s, %s, 
                    %s, %s, %s, %s, 
                    %s, %s, %s, %s, 
                    %s, %s, %s, %s
                );
                """

                editTuple = (
                    added,
                    deleted,
                    editDate,
                    editId,
                    pageId,
                    insInternalLink,
                    insExternalLink,
                    insLongestInsertedWord,
                    insLongestCharacterSequence,
                    insPronouns,
                    insCapitalization,
                    insDigits,
                    insSpecialChars,
                    insWhitespace,
                    delWords,
                    commentPersonalLife,
                    commentCopyedit,
                    commentLength,
                    commentSpecialChars,
                    blanking,
                    userTableId,
                )

                ## Insert page features into database
                cursor.execute(query, editTuple)

                # oldrevision = revision

        ## Change status of dump
        currenttime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        query = """UPDATE partition
            SET status = "done", end_time_1 = %s 
            WHERE file_name = %s;"""
        cursor.execute(query, (currenttime, filename))

    except:
        err = str(sys.exc_info()[1])

        currenttime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        print(err)

        query = """UPDATE partition
            SET
                status = "failed",
                end_time_1 = %s,
                error = %s
            WHERE
                file_name = %s;"""
        cursor.execute(query, (currenttime, err, filename))

        raise

    cursor.close()
    database.close()


if __name__ == "__main__":
    parse()
