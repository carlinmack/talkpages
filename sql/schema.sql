-- Manually created, but 90% there
-- See annotated version at https://docs.google.com/spreadsheets/d/16aVV2Wh7ezjwaGnoYtqU9bVHTXNBPwHwhX3oVctmTUM/edit#gid=1860359731
DROP TABLE edit;

DROP TABLE page;

DROP TABLE user;

CREATE TABLE user (
    id int unsigned NOT NULL AUTO_INCREMENT,
    user_id int DEFAULT NULL,
    username varchar(85) DEFAULT NULL,
    ip_address varbinary(16) DEFAULT NULL,
    confirmed tinyint(1) DEFAULT NULL,
    user_special tinyint(1) DEFAULT NULL,
    bot tinyint(1) DEFAULT NULL,
    blocked tinyint(1) DEFAULT NULL,
    paid tinyint(1) DEFAULT NULL,
    user_page tinyint(1) DEFAULT NULL,
    user_talkpage tinyint(1) DEFAULT NULL,
    number_of_edits int unsigned NOT NULL DEFAULT '0',
    reverted_edits int unsigned DEFAULT NULL,
    namespaces set('0','1','2','3','4','5','6','7','8','9','10','11','12','13',
        '14','15','-1','-2','100','101','118','119','710','711','828','829',
        '108','109','446','447','2300','2301','2302','2303') NOT NULL DEFAULT '',
    PRIMARY KEY (id),
    UNIQUE KEY user_id_UNIQUE (user_id),
    UNIQUE KEY ip_address_UNIQUE (ip_address)
);

/* check and trigger ? */
CREATE TABLE page (
    page_id INT NOT NULL,
    namespace smallint NOT NULL,
    title TEXT,
    file_name varchar(85) NOT NULL,
    PRIMARY KEY (page_id)
);

CREATE TABLE edit (
    id int unsigned NOT NULL AUTO_INCREMENT,
    edit_id int DEFAULT NULL,
    edit_date datetime NOT NULL,
    page_id int DEFAULT NULL,
    user_table_id int unsigned NOT NULL,
    added text,
    deleted text,
    blanking tinyint(1) DEFAULT NULL,
    comment_copyedit tinyint(1) DEFAULT NULL,
    comment_length tinyint(1) DEFAULT NULL,
    comment_personal_life tinyint(1) DEFAULT NULL,
    comment_spam decimal(4, 4) DEFAULT NULL,
    comment_special_chars decimal(4, 4) DEFAULT NULL,
    del_bias mediumint DEFAULT NULL,
    del_words mediumint DEFAULT NULL,
    ins_bias decimal(4, 4) DEFAULT NULL,
    ins_capitalization decimal(4, 4) DEFAULT NULL,
    ins_compressibility decimal(4, 4) DEFAULT NULL,
    ins_digits decimal(4, 4) DEFAULT NULL,
    ins_external_link smallint DEFAULT NULL,
    ins_internal_link smallint DEFAULT NULL,
    ins_longest_character_sequence smallint DEFAULT NULL,
    ins_longest_inserted_word smallint DEFAULT NULL,
    ins_pronouns decimal(4, 4) DEFAULT NULL,
    ins_sex decimal(4, 4) DEFAULT NULL,
    ins_special_chars decimal(4, 4) DEFAULT NULL,
    ins_special_words decimal(4, 4) DEFAULT NULL,
    ins_vulgarism decimal(4, 4) DEFAULT NULL,
    ins_whitespace decimal(4, 4) DEFAULT NULL,
    ins_wp decimal(4, 4) DEFAULT NULL,
    kldnew2old decimal(5, 5) DEFAULT NULL,
    PRIMARY KEY (id),
    KEY user_idx (user_table_id),
    KEY page_idx (page_id),
    CONSTRAINT page FOREIGN KEY (page_id) REFERENCES page (page_id),
    CONSTRAINT user FOREIGN KEY (user_table_id) REFERENCES user (id)
);

CREATE TABLE partition (
    id int unsigned NOT NULL AUTO_INCREMENT,
    file_name varchar(85) NOT NULL,
    status enum('todo','running','failed','restarted','failed again','done',
        'cleaned') NOT NULL DEFAULT 'todo',
    error text,
    start_time_1 timestamp NULL DEFAULT '0000-00-00 00:00:00',
    end_time_1 timestamp NULL DEFAULT '0000-00-00 00:00:00',
    start_time_2 timestamp NULL DEFAULT '0000-00-00 00:00:00',
    end_time_2 timestamp NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (id)
);